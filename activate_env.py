import subprocess
import os

# Ativa o ambiente virtual
subprocess.run([r'.\venv\Scripts\activate.bat'], shell=True)

# Configura as variáveis de ambiente
os.environ['PATH'] = r'.\bin;' + os.environ['PATH']
os.environ['PYTHONUTF8'] = '1'